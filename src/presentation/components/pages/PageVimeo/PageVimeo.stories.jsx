import PageVimeo from "./PageVimeo";

export default {
  title: "Pages/PageVimeo",
  component: PageVimeo,
};

const Template = (args) => <PageVimeo {...args} />;

export const PrimaryPageVimeo = Template.bind({});
