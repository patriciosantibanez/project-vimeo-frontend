import styled from "styled-components";
import CardMUI from "@mui/material/Card";

const Card = styled(CardMUI)`
  background: #ffffff;
  box-shadow: 0px 0px 5px 5px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
  width: 100%;
  margin: 5% 10% 0% 10%;
`;

export default Card;
