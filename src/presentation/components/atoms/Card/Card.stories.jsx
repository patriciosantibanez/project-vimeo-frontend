import Card from "./Card";

//import { withDesign } from 'storybook-addon-designs'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Atoms/Card",
  component: Card, //decorators: [withDesign],
};

const Template = (args) => <Card title="Test title" {...args} />;

export const PrimaryCard = Template.bind({});
