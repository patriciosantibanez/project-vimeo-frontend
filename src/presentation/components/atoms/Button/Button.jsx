import ButtonMUI from "@mui/material/Button";
import styled from "styled-components";
import SearchIcon from "@mui/icons-material/Search";
import { func } from "prop-types";

const StyleButton = styled(ButtonMUI)`
  && {
    margin-left: 25%;
    background-color: #ffffff;
    box-shadow: none;
    border-radius: 0px 0px 0px 0px;
    :hover {
      background: #e7e7e7;
      box-shadow: none;
    }
  }
`;
const StyleSearchIcon = styled(SearchIcon)`
  && {
    color: #1a2e3b;
    width: 70%;
  }
`;

const Button = (onClick, ...props) => {
  return (
    <StyleButton variant="contained" onClick={onClick} {...props}>
      <StyleSearchIcon />
    </StyleButton>
  );
};
Button.propTypes = {
  onClick: func,
};

export default Button;
