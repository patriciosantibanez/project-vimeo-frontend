import Button from "./Button";

//import { withDesign } from 'storybook-addon-designs'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Atoms/Button",
  component: Button, //decorators: [withDesign],
};

const Template = (args) => <Button title="Test title" {...args} />;

export const PrimaryButton = Template.bind({});
