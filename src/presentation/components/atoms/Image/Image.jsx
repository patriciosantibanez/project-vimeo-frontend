import styled from "styled-components";

const Image = styled("img")`
  width: 50%;
  height: auto;
`;

export default Image;
