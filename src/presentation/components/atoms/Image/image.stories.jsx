import Image from "./Image";
import imagen from "../../../assets/img/imagen.jpg";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Atoms/Image",
  component: Image,
};

export const Opacity = () => <Image src={imagen} alt="imagen" />;

export const Large = () => <Image size="large" src={imagen} alt="imagen" />;
