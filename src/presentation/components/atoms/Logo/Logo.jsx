import styled from "styled-components";
import logo from "../../../assets/img/vimeologo.svg";

const Img = styled("img")`
  width: 150px;
  height: 70px;
`;

const Logo = () => {
  return <Img src={logo} alt="logo" />;
};

export default Logo;
