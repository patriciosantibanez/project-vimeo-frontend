import styled from "styled-components";
import TextFieldMui from "@mui/material/TextField";
import { string } from "prop-types";

const Input = styled(TextFieldMui)`
  .MuiOutlinedInput-root {
    width: 40%;
    height: 40px;
    margin: 25px;
    font-size: 15px;
    border-color: #7c7c7c;
    & fieldset {
      border-color: #d1d0d0;
    }
  }
  .Mui-disabled {
    cursor: not-allowed;
  }
  .customtextfield {
    & input::placeholder {
      font-size: 15px;
    }
  }
`;

const Field = ({ name, onChange, ...props }) => {
  return (
    <Input
      fullWidth
      variant="outlined"
      type="search"
      InputLabelProps={{ shrink: false }}
      id={name}
      name={name}
      onChange={onChange}
      {...props}
    />
  );
};

Field.propTypes = {
  placeholder: string,
  name: string,
};
Field.defaultProps = {
  placeholder: "",
};

export default Field;
