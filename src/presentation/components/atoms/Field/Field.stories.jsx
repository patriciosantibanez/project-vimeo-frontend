import Field from "./Field";

//import { withDesign } from 'storybook-addon-designs'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Atoms/Field",
  component: Field, //decorators: [withDesign],
};

const Template = (args) => <Field title="Test title" {...args} />;

export const PrimaryField = Template.bind({});
