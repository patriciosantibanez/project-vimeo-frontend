import styled from 'styled-components'
import TitleCard from './TitleCard'

const TextCard = styled(TitleCard)`
  font-size: 14px;
  line-height: 16px;
  font-weight: normal;
`
export default TextCard
