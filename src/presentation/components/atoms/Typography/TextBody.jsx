import styled from 'styled-components'
import Text from './Text'

const TextBody = styled(Text)`
  font-size: 18px;
  line-height: 32px;
  letter-spacing: 0.2px;
  font-style: normal;
  font-weight: normal;
`

export default TextBody
