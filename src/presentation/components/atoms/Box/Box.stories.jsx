import Box from "./Box";

//import { withDesign } from 'storybook-addon-designs'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Atoms/Box",
  component: Box, //decorators: [withDesign],
};

const Template = (args) => <Box title="Test title" {...args} />;

export const PrimaryBox = Template.bind({});
