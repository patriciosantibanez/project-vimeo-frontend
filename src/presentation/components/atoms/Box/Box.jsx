import BoxMUI from "@mui/material/Box";
import styled from "styled-components";

const Box = styled(BoxMUI)`
  width: 100%;
`;

export default Box;
