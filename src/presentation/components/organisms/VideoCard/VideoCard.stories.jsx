import VideoCard from "./VideoCard";

export default {
  title: "Organisms/VideoCard",
  component: VideoCard,
};

const Template = (args) => <VideoCard {...args} />;

export const PrimaryVideoCard = Template.bind({});
