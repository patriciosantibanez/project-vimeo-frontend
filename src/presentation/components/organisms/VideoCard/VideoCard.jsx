import ContainerCard from "../../molecules/ContainerCard";
import Box from "../../atoms/Box";
import imagen from "../../../assets/img/imagen.jpg";
import styled from "styled-components";

const BoxStyle = styled(Box)`
  width: 45%;
  display: column;
`;

const data = [
  {
    videoImage: imagen,
    videoId: "a",
    title: "Titulo",
    subtitle: "Descripción",
  },
  {
    videoImage: imagen,
    videotId: "b",
    title: "Titulo",
    subtitle: "Descripción",
  },
  {
    videoImage: imagen,
    videotId: "c",
    title: "Titulo",
    subtitle: "Descripción",
  },
  {
    videoImage: imagen,
    videotId: "d",
    title: "Titulo",
    subtitle: "Descripción",
  },
  {
    videoImage: imagen,
    videotId: "e",
    title: "Titulo",
    subtitle: "Descripción",
  },
];

const VideoCard = () => {
  return (
    <BoxStyle>
      {data.map((video) => (
        <ContainerCard
          key={video.videotId}
          videoImage={video.videoImage}
          videoName={video.videotName}
          title={video.title}
          subtitle={video.subtitle}
        />
      ))}
    </BoxStyle>
  );
};

export default VideoCard;
