import Logo from "../../atoms/Logo";
import Box from "../../atoms/Box";
import SearchInput from "../../molecules/SearchInput";
import styled from "styled-components";
import { func, string } from "prop-types";

const StyledBox = styled(Box)`
  width: 100%;
  margin-left: 5%;
`;

const StyledLogo = styled(Logo)`
  width: 25%;
  margin-left: 100%;
`;

const StyledSearchInput = styled(SearchInput)`
  margin: 90%;
`;

const Header = ({ name, onClick, onChange }) => {
  return (
    <StyledBox>
      <StyledLogo />
      <StyledSearchInput
        name={name}
        placeholder="Buscar videos, personas y más"
        onChange={onChange}
        onClick={onClick}
      />
    </StyledBox>
  );
};

Header.propTypes = {
  name: string,
  onClick: func,
  onChange: func,
};

export default Header;
