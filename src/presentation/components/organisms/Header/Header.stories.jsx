import Header from "./Header";

export default {
  title: "Organisms/Header",
  component: Header,
};

const Template = (args) => <Header {...args} />;

export const PrimaryHeader = Template.bind({});
