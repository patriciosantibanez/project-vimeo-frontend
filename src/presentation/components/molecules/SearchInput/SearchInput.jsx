import Field from "../../atoms/Field";
import InputAdornment from "@mui/material/InputAdornment";
import { string } from "prop-types";
import Button from "../../atoms/Button";
import styled from "styled-components";

const StyleTextField = styled(Field)`
  .MuiOutlinedInput-root {
    margin-top: -50px;
    margin-left: 50%;
  }
`;

const SearchInput = ({ name, placeholder, onClick, ...props }) => {
  return (
    <>
      <StyleTextField
        fullWidth
        name={name}
        placeholder={placeholder}
        {...props}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Button onClick={onClick} />
            </InputAdornment>
          ),
        }}
        onKeyDown={(event) => {
          if (event.key === "Enter") {
            onClick();
          }
        }}
      />
    </>
  );
};

SearchInput.propTypes = {
  placeholder: string,
  name: string,
};

export default SearchInput;
