import SearchInput from "./SearchInput";

export default {
  title: "Molecules/SearchInput",
  component: SearchInput,
};

const Template = (args) => <SearchInput {...args} />;

export const PrimarySearchInput = Template.bind({});
