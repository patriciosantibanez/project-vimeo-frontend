import ContainerCard from "./ContainerCard";

export default {
  title: "Molecules/ContainerCard",
  component: ContainerCard,
};

const Template = (args) => <ContainerCard {...args} />;

export const PrimaryContainerCard = Template.bind({});
