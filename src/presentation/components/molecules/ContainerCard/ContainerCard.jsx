import Card from "../../atoms/Card";
import Image from "../../atoms/Image";
import TitleCard from "../../atoms/Typography/TitleCard";
import SubTitleCard from "../../atoms/Typography/SubtitleCard";
import Box from "../../atoms/Box";
import { string } from "prop-types";
import imagen from "../../../assets/img/imagen.jpg";
import styled from "styled-components";

const BoxStyle = styled(Box)`
  margin-top: auto;
  margin-left: 2%;
  width: 100%;
`;

const ImageStyle = styled(Image)`
  width: 40%;
  top: 10%;
  margin-top: 1%;
  margin-left: 2%;
`;

const ContainerCard = ({ variantImage, title, text }) => {
  return (
    <Card>
      <ImageStyle src={imagen} alt="base photo" />
      <BoxStyle>
        <TitleCard title={title}>{title}</TitleCard>
      </BoxStyle>
      <BoxStyle>
        <SubTitleCard text={text}>{text}</SubTitleCard>
      </BoxStyle>
    </Card>
  );
};

ContainerCard.propTypes = {
  title: string,
  text: string,
};

export default ContainerCard;
