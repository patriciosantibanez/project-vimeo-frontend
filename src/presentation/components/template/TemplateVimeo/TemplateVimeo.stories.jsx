import TemplateVimeo from "./TemplateVimeo";

export default {
  title: "Template/TemplateVimeo",
  component: TemplateVimeo,
};

const Template = (args) => <TemplateVimeo {...args} />;

export const PrimaryTemplateVimeo = Template.bind({});
