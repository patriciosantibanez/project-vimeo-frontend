import Box from "../../atoms/Box";
import Header from "../../organisms/Header";
import VideoCard from "../../organisms/VideoCard";
import styled from "styled-components";
import { useState, useCallback, useEffect } from "react";

const BoxStyle = styled(Box)`
  margin-left: 4%;
`;

const SearchTemplate = () => {
  const [submitting, setSubmitting] = useState(true);
  const [data, setData] = useState([]);
  const [query, setQuery] = useState(
    "https://api.vimeo.com/videos?query=futbol"
  );
  const [name, setName] = useState("button-search");

  const getData = useCallback(async () => {
    console.log(query);
    if (query) {
      const response = await fetch(
        `http://localhost:3001/search/list/${query}`
      );
      const data = await response.json();
      const videos = [];
      data.items.map(({ id, snippet }) => {
        const elemento = {
          variantId: id.videoId,
          variantImage: snippet.thumbnails.default.url,
          title: snippet.title,
          text: snippet.description.substring(0, 100),
        };
        console.log(snippet.thumbnails.default.url);
        videos.push(elemento);
        setData(videos);
      });

      console.log(data);
    }
  }, [query]);

  useEffect(() => {
    if (submitting) {
      // is true initially, and again when button is clicked
      getData().then(() => setSubmitting(false));
    }
  }, [submitting, getData]);

  const handleChange = (event) => {
    event.preventDefault();
    setQuery(event.target.value);
  };

  return (
    <>
      <BoxStyle>
        <Header
          name={name}
          onChange={handleChange}
          onClick={() => setSubmitting(true)}
        />
      </BoxStyle>
      <VideoCard data={data} />
    </>
  );
};

export default SearchTemplate;
