import { Routes as AppRoutes, Route } from "react-router-dom";
import PageVimeo from "../components/pages/PageVimeo";

const Routes = () => {
  return (
    <AppRoutes>
      <Route path="/search" element={<PageVimeo />} />;
    </AppRoutes>
  );
};

export default Routes;
