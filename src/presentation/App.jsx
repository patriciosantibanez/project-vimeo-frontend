import { BrowserRouter } from "react-router-dom";
import Routes from "./routes/Routes";

function App() {
  return (
    <BrowserRouter basename={"/"}>
      <Routes />
    </BrowserRouter>
  );
}

export default App;
